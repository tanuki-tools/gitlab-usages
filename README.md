# gitlab-usages

## How to

- First, get the `production_json.log` file from `/var/log/gitlab/gitlab-rails/` on your GitLab instance
- Then, copy it to `/logs`
- Finally, launch `node gen-json.js` and retrieve the reports in the `/reports` directory

You can view examples by browsing the jobs artifacts

