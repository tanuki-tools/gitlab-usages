
let html_report = (title, requests) => `
<style>
  .center {
    text-align: center;
  }

  th {
    position: -webkit-sticky;
    position: sticky;
    top: 0;
    z-index: 2;
  }

  #features {
    font-family: Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }
  
  #features td, #features th {
    border: 1px solid #ddd;
    padding: 8px;
  }
  
  #features tr:nth-child(even){background-color: #f2f2f2;}
  
  #features tr:hover {background-color: #ddd;}
  
  #features th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #6d49cb;
    color: white;
  }

</style>
<h2 style="font-family: Arial, Helvetica, sans-serif;">${title}</h2>
<hr>
<table id="features">
  <thead>
    <tr>
      <th>id</th>
      <th>controller</th>
      <th>method</th>
      <th>path</th>
      <th>action</th>
      <th>user_id</th>
      <th>username</th>
      <th>time</th>
    </tr>
  </thead>
  <tbody>
    ${requests.map((request, index) => 
      `
        <tr>
          <td>${index+1}</td>
          <td>${request.controller}</td>
          <td>${request.method}</td>
          <td>${request.path}</td>
          <td>${request.action}</td>
          <td>${request.user_id}</td>
          <td>${request.username}</td>
          <td>${request.time}</td>
        </tr>
      `
    ).join("")}
  </tbody>
</table>
`


let html_group_by_report = (title, controllers) => `
<style>
  .center {
    text-align: center;
  }

  th {
    position: -webkit-sticky;
    position: sticky;
    top: 0;
    z-index: 2;
  }

  #features {
    font-family: Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }
  
  #features td, #features th {
    border: 1px solid #ddd;
    padding: 8px;
  }
  
  #features tr:nth-child(even){background-color: #f2f2f2;}
  
  #features tr:hover {background-color: #ddd;}
  
  #features th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #6d49cb;
    color: white;
  }

</style>
<h2 style="font-family: Arial, Helvetica, sans-serif;">${title}</h2>
<hr>
<table id="features">
  <thead>
    <tr>
      <th>Controllers</th>
      <!--<th>controller</th>-->
      <th>method</th>
      <th>path</th>
      <th>action</th>
      <th>user_id</th>
      <th>username</th>
      <th>time</th>
    </tr>
  </thead>
  <tbody>

    ${controllers.map(item =>
      `
        <tr><td><b>${item.controller}</b></td></tr>
        ${item.requests.map((request, index) => 
          `
            <tr>
              <td>${index+1}</td>
              <!--<td>${request.controller}</td>-->
              <td>${request.method}</td>
              <td>${request.path}</td>
              <td>${request.action}</td>
              <td>${request.user_id}</td>
              <td>${request.username}</td>
              <td>${request.time}</td>
            </tr>
          `
        ).join("")}       
      `
    ).join("")}


  </tbody>
</table>
`

module.exports = {
  htmlReport: html_report,
  htmlGroupByReport: html_group_by_report
}