const fs = require('fs')
const _ = require('lodash')
const reports = require('./html-reports')

const path_reports = "./reports"
const path_logs = "./logs"

// ---------------------------------------------------------------
// get all requests of all controllers from production_json.log
// you can find the logfile here: /var/log/gitlab/gitlab-rails/
// ---------------------------------------------------------------
let controllersRequests = require('fs').readFileSync(`${path_logs}/production_json.log`, 'utf-8')
    .split('\n')
    .filter(Boolean)
    .map(line => JSON.parse(line))

// ---------------------------------------------------------------
// create a json file of all the requests
// ---------------------------------------------------------------
fs.writeFileSync(`${path_reports}/all_controllers_requests.json`, JSON.stringify(controllersRequests), 'utf8')

// ---------------------------------------------------------------
// create a "simplified" json file of all the requests
// with only some keys of the logs
// ---------------------------------------------------------------
let firstGardening = controllersRequests.filter(item => item.user_id !== null).map(item => {
  return {
    controller: item.controller,
    method: item.method,
    path: item.path,
    action: item.action,
    user_id: item.user_id,
    username: item.username,
    time: item.time
  }
})

fs.writeFileSync(`${path_reports}/simplified_all_controllers_requests.json`, JSON.stringify(firstGardening, null, 4), 'utf8')
// ---------------------------------------------------------------
// create an html report of the controllers requests
// ---------------------------------------------------------------
fs.writeFileSync(`${path_reports}/simplified_all_controllers_requests.html`, reports.htmlReport("Requests", firstGardening))

// ---------------------------------------------------------------
// create an html report of the controllers requests
// group by controller's name
// ---------------------------------------------------------------
let secondGardening = _.chain(firstGardening)
  .groupBy("controller")
  .map((value, key) => {
    return {
      controller: key,
      requests: value
    }
  })
  .value()

fs.writeFileSync(`${path_reports}/groupby_controllers_requests.json`, JSON.stringify(secondGardening, null, 4), 'utf8')
fs.writeFileSync(`${path_reports}/groupby_controllers_requests.html`, reports.htmlGroupByReport("Requests", secondGardening))
